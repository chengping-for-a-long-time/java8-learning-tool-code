# Java8学习工具代码

#### 介绍
Java8新特新学习过程中对于lambda设计应用的理解，及Stream工作流设计应用的理解

# lambda
lambda只能作用于函数接口，即一个接口inface中有且只有一个方法。其实现过程相当于Java的匿名内部类，只是匿名内部类会创建一个新的实例，而lambda则不会创建新的实例，二者的作用结识对原接口方法的重写，lambda表达式重方法:  (参数) -> 方法（操作参数）

# Stream
本质上是对List操作时，使用如stream().filter,  stream().map等方法时，利用的是对filter和map方法内置的函数接口方法的重写来实现的。

# Util
这个工具类是对以上两种方式的总结，实现了List<T>转Map<K,T>的方法，和转化时进行过滤的方法，同时对group by 也进行了模拟实现



#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


