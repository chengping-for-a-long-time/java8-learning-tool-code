package com.fsp;

import org.junit.jupiter.api.Test;

import java.util.function.Consumer;

/**
 * @author fushengping
 * @className FunctionTest
 * @description
 * @date 2021/12/1 16:32
 */
public class FunctionTest {

    @Test
    public void test1() {

        /*匿名函数重写,生成一个匿名内部类*/
        happyTime(500, new Consumer<Double>() {
            @Override
            public void accept(Double aDouble) {
                System.out.println("消费了"+aDouble);
            }
        });

        /*lambda重写该方法*/
        happyTime(300,money -> System.out.println("使用了"+ money));
    }

    public void happyTime(double money, Consumer<Double> con) {
        con.accept(money);
    }
}
