package com.fsp.demoStreamMethodReference02;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author fushengping
 * @className Person
 * @description
 * @date 2021/12/1 14:44
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Person {
    private Integer age;
    // 改动1：新增一个方法，逻辑和之前案例的Lambda表达式相同
    public static int compare(Person p1,Person p2) {
        return p1.getAge() - p2.getAge();
    }
}
