package com.fsp.demoStreamMethodReference02;

import java.util.ArrayList;
import java.util.List;

/**
 * @author fushengping
 * @className MethodResference
 * @description
 * @date 2021/12/1 14:46
 */
public class MethodReference {
    private static final List<Person> list;
    static {
        list = new ArrayList<>();
        list.add(new Person(19));
        list.add(new Person(15));
        list.add(new Person(18));
    }

    public static void main(String[] args) {
        System.out.println(list);
        // 改动2：既然Person内部有个逻辑一样的方法，就用它来替换Lambda
        list.sort(Person::compare);
        System.out.println(list);
    }
}
