package com.fsp.ConvertUtil;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author fushengping
 * @className Person
 * @description
 * @date 2021/12/2 9:55
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Person {
    private String name;
    private Integer age;
    private String address;
    private Double salary;

}
