package com.fsp.ConvertUtil;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author fushengping
 * @className TestController
 * @description
 * @date 2021/12/2 9:59
 */
@RestController
public class TestController {

    @Autowired
    private ConvertUtil convertUtil;

    @GetMapping("/test")
    public Map<String,Person> test() {
        Map<String, Person> map = convertUtil.getMap();
        return map;
    }
}
