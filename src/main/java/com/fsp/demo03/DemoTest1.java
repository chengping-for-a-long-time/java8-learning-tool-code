package com.fsp.demo03;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author fushengping
 * @className DemoTest1
 * @description
 * @date 2021/11/30 17:35
 */
public class DemoTest1 {
    public static void main(String[] args) {
        //原始数据
        List<Integer> list = new ArrayList<>(Arrays.asList(1, 2, 3));
        // filter()方法需要传入一个过滤逻辑
        List<Integer> result = list.stream().filter(value -> value > 2).collect(Collectors.toList());
        System.out.println(result);
    }
}
