package com.fsp.demoStreamMethodReference01;

import java.util.ArrayList;
import java.util.List;

/**
 * @author fushengping
 * @className MethodReference
 * @description
 * @date 2021/12/1 14:24
 */
public class MethodReference {
    private static final List<Person> list;

    static {
        list = new ArrayList<>();
        list.add(new Person(15));
        list.add(new Person(19));
        list.add(new Person(17));
        list.add(new Person(16));
    }

    public static void main(String[] args) {
        System.out.println(list);
        // sort()方法是List本身就有的，主要用来排序
        list.sort((p1,p2) -> p1.getAge() - p2.getAge());
        System.out.println(list);
    }
}
