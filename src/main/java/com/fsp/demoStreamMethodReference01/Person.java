package com.fsp.demoStreamMethodReference01;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author fushengping
 * @className Person
 * @description
 * @date 2021/12/1 14:23
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Person {
    private Integer age;
}
