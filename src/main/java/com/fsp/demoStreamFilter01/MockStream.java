package com.fsp.demoStreamFilter01;

/**
 * @author fushengping
 * @className MockStream
 * @description 测试predication接口
 * @date 2021/12/1 9:34
 */
public class MockStream {
    public static void main(String[] args) {
        Person fsp = new Person("fsp",18);

        /*1.具体实现类(多态),该接口的实现类中包含了函数的逻辑运算*/
        Predicate<Person> predicate1 = new PredicateImpl();
        myPrint(fsp,predicate1);

        /*2.匿名实现类,实现该接口并重写*/
        Predicate<Person> predicate2 = new Predicate<Person>(){
            @Override
            public boolean test(Person person) {
                return person.getAge() < 18;
            }
        };
        myPrint(fsp,predicate2);

        /*3.lambda表达式,重写了这个接口的唯一方法，但其并未创建一个类，而匿名内部类则是又创建了一个新的类*/
        Predicate<Person> predicate3 = person -> person.getAge() == 18;
        myPrint(fsp,predicate3);
        /*
        * // 1.普通的方法
            public boolean test() {
                // 啥逻辑都没有，直接返回true，你也可以按照实际业务写，比如age>18
                return true;
            }

          // 2.Lambda形式
            () -> return true;
        * */

    }

    /**
     * 用Predicate函数式接口先行占坑。我们无需关注Predicate传入myPrint()后将被如何调用，只要关注如何实现Predicate
     * @param person
     * @param filter
     */
    public static void myPrint(Person person, Predicate<Person> filter) {
        if (filter.test(person)) {
            System.out.println("true");
        } else {
            System.out.println("false");
        }
    }
}
