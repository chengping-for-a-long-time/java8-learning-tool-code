package com.fsp.streamAPI.demo01;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author fushengping
 * @className StreamTest01
 * @description collect()、Collector、Collectors
 * @date 2021/12/2 16:14
 */
public class StreamTest01 {
    private static List<Person> list;

    static {
        list = new ArrayList<>();
        list.add(new Person("甲", 18, "杭州", 999.9));
        list.add(new Person("乙", 19, "温州", 777.7));
        list.add(new Person("丙", 21, "杭州", 888.8));
        list.add(new Person("丁", 17, "宁波", 888.8));

    }

    public static void main(String[] args) {
        List<Person> collect = list.stream().filter(person -> person.getAge() > 18).collect(Collectors.toList());
        System.out.println(collect);
    }

}
