package com.fsp.streamAPI.demo01;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author fushengping
 * @className Person
 * @description
 * @date 2021/12/2 16:15
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Person {
    private String name;
    private Integer age;
    private String address;
    private Double salary;
}
