package com.fsp.demo02;

import java.util.Comparator;

/**
 * @author fushengping
 * @className DemoTest01
 * @description
 * @date 2021/11/30 17:13
 */
public class DemoTest01 {
    public static void main(String[] args) {
        String str1 = "abc";
        String str2 = "abcd";

        int i = compareString(str1, str2, new Comparator<String>() {
            @Override
            public int compare(String s1, String s2) {
                return s1.length() - s2.length();
            }
        });
    }

    public static int compareString(String str1, String str2, Comparator<String> comparator) {
        return comparator.compare(str1, str2);
    }
}
