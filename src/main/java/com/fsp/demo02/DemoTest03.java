package com.fsp.demo02;

import java.util.Comparator;

/**
 * @author fushengping
 * @className DemoTest03
 * @description 推导3：
 * @date 2021/11/30 17:20
 */
public class DemoTest03 {
    public static void main(String[] args) {
        String str1 = "abc";
        String str2 = "abcd";

        /*改进一下，跳过赋值这一步，直接把整个Lambda传给compareString()方法：*/
        int i = compareString(str1,str2,(String s1,String s2)->{
            return s1.length() - s2.length();
        });
        System.out.println(i);

        /*经过上述改进，仍有灰色代码,还有改进的空间*/
        int x = compareString(str1,str2,(s1,s2)-> s1.length() - s2.length());
        System.out.println(x);

        /*经过上述修改，仍有灰色代码，还是不够精简，再改改（方法引用）*/
        int y = compareString(str1,str2,Comparator.comparingInt(String::length));
        System.out.println(y);
//        int i = compareString(str1, str2, new Comparator<String>() {
//            @Override
//            public int compare(String s1, String s2) {
//                return s1.length() - s2.length();
//            }
//        });
    }

    public static int compareString(String str1, String str2, Comparator<String> comparator) {
        return comparator.compare(str1, str2);
    }
}
