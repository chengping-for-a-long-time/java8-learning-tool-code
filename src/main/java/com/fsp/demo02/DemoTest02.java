package com.fsp.demo02;

import java.util.Comparator;

/**
 * @author fushengping
 * @className DemoTest02
 * @description 推导一：
 * @date 2021/11/30 17:15
 */
public class DemoTest02 {
    public static void main(String[] args) {
        String str1 = "abc";
        String str2 = "abcd";

        /*推导一：*/
        // 上面推导得出Lambda表达式与匿名类对象等价，所以我们可以把Lambda表达式赋值给Comparator接口
        Comparator<String> comparator = (String s1,String s2) -> {
            return s1.length() - s2.length();
        };
        //调用
        int i = compareString(str1,str2,comparator);

//        int i = compareString(str1, str2, new Comparator<String>() {
//            @Override
//            public int compare(String s1, String s2) {
//                return s1.length() - s2.length();
//            }
//        });
    }

    public static int compareString(String str1, String str2, Comparator<String> comparator) {
        return comparator.compare(str1, str2);
    }
}
