package com.fsp.demoStreamMap01;

/**
 * @author fushengping
 * @className MockStream
 * @description
 * @date 2021/12/1 11:14
 */
public class MockStream {
    public static void main(String[] args) {
        Person fsp = new Person("fsp", 18);

        /*具体实现类模式(多态：将实现类当作接口来使用)*/
        Function<Person,Integer> function1 = new FunctionImpl();
        myPrint(fsp,function1);
        System.out.println("\n-----------------------------------\n");

        /*使用匿名类来实现(匿名类重写该接口的方法)，相当于创建一个新的类*/
        Function<Person,Integer> function2 = new Function<Person, Integer>() {
            @Override
            public Integer apply(Person person) {
                return person.getAge();
            }
        };
        myPrint(fsp,function2);
        System.out.println("\n-----------------------------------\n");

        /*使用lambda表达式重写该接口但不创建新的类(person -> person.getAge();只是重写了接口中的方法)*/
        Function<Person,Integer> function3 = person -> person.getAge();
        myPrint(fsp,function3);

    }

    /**
     * 用Predicate函数式接口先行占坑。我们无需关注Function传入myPrint()后将被如何调用，只要关注如何实现Predicate
     * @param person
     * @param mapper
     */
    public static void myPrint(Person person, Function<Person, Integer> mapper) {
        System.out.println(mapper.apply(person));//这里调用了接口的方法
    }
}
