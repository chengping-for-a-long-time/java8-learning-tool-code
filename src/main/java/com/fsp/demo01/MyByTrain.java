package com.fsp.demo01;

/**
 * @author fushengping
 * @className MyRunnByTain
 * @description
 * @date 2021/11/30 15:54
 */
public class MyByTrain implements MyRunnable {
    @Override
    public void runs() {
        System.out.println("买了一张火车票！");
        System.out.println("坐火车！");
    }
}
