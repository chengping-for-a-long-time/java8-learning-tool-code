package com.fsp.demo01;

/**
 * @author fushengping
 * @className MyByAir
 * @description
 * @date 2021/11/30 15:57
 */
public class MyByAir implements MyRunnable {
    @Override
    public void runs() {
        System.out.println("订机票！");
        System.out.println("坐飞机！");
    }
}
