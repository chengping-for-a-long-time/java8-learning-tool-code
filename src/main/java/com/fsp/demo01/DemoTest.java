package com.fsp.demo01;

/**
 * @author fushengping
 * @className DemoTest
 * @description 对策略模式优化的调用
 * @date 2021/11/30 16:08
 */
public class DemoTest {
    public static void main(String[] args) {
        new MyThread().start();
        System.out.println("========================");

        MyByTrain myByTrain = new MyByTrain(); //策略实例，他实现了公共接口
        new MyThread(myByTrain).start(); //这里使用了MyThread的有参构造，将策略传递给了MyThread中
        System.out.println("========================");

        /*匿名类实现的是类则为对该类的继承，实现的是接口则是对该接口的实现(都会对其进行重写)*/
        //若策略增加，将导致”类爆炸“使用匿名类的方式继续改进Demo
        new MyThread(new MyRunnable() {  //这里直接将MyRunnable接口传入，重写了runs()方法
            @Override
            public void runs() {
                System.out.println("路程近！");
                System.out.println("开车去！");
            }
        }).start();
        System.out.println("========================");

        /*lambda就是相当于匿名类*/
        new MyThread(()->
            System.out.println("测试lambda")).start();
    }
}
