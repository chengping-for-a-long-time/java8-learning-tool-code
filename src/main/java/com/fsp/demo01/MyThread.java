package com.fsp.demo01;

/**
 * @author fushengping
 * @className MyThread
 * @description 实例类，通过构造函数接收不同实现类,并调用该方法
 * @date 2021/11/30 15:59
 */
public class MyThread {
    //将多个实现类的接口作为一个成员变量
    private MyRunnable target;

    //无参构造
    public MyThread() {
    }

    //有参构造,接收外部传递的出行策略(多个该接口的实现类)
    public MyThread(MyRunnable target) {
        this.target = target;
    }

    //MyThread自己的run方法
    public void run() {
        System.out.println("无交通工具！");
        System.out.println("步行！");
    }

    //如果外部传递(调用)了出行策略，即调用了公共接口的多个实现类，即实现出行策略的方法
    public void start() {
        if (target != null) {
            target.runs();
        } else {
            this.run();
        }
    }
}
