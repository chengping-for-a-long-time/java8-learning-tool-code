package com.fsp.demo01;

/**
 * @author fushengping
 * @className MyBus
 * @description
 * @date 2021/11/30 15:56
 */
public class MyByBus implements MyRunnable {
    @Override
    public void runs() {
        System.out.println("买了公交票！");
        System.out.println("坐公交！");
    }
}
