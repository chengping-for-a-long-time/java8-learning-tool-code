package com.fsp.demo01;

/**
 * @author fushengping
 * @interface MyRunnable
 * @description 定义一个接口，令多个实现类来实现该接口，从而实现方法的传递(通过实现类的构造函数进行传递)
 * @date 2021/11/30 15:52
 */
public interface MyRunnable {
    void runs();
}
