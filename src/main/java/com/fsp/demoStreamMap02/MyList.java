package com.fsp.demoStreamMap02;

import java.util.ArrayList;
import java.util.List;

/**
 * @author fushengping
 * @className MyList
 * @description
 * @date 2021/12/1 13:49
 */
public class MyList<T> {
    private List<T> list = new ArrayList<>();

    // 1.外部调用add()添加的元素都会被存在list
    public boolean add(T t) {
        return list.add(t);
    }

    /**
     * 把MyList中的List<T>转为List<R>
     * 不要关注Function<T, R>接口本身，而应该关注apply()
     * apply()接收T t，返回R t。具体实现需要我们从外面传入，这里只是占个坑
     * @param mapper
     * @param <R>
     * @return
     */

    public <R> List<R> map(Function<T,R> mapper) {
        List<R> mapList = new ArrayList<>();
        for (T t : list) {
            // mapper通过apply()方法把T t 变成 R t，然后加入到新的list中
            mapList.add(mapper.apply(t));
        }
        return mapList;
    }
}
