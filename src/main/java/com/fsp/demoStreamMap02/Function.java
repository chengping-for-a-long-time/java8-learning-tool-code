package com.fsp.demoStreamMap02;

/**
 * @author fushengping
 * @interface Function
 * @description 唯一函数接口
 * @date 2021/12/1 13:46
 */
@FunctionalInterface
public interface Function<E,R> {
    /**
     * 传入元素E,返回元素类型为E
     * @param e
     * @return
     */
    R apply(E e);
}
