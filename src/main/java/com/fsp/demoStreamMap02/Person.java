package com.fsp.demoStreamMap02;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author fushengping
 * @className Person
 * @description
 * @date 2021/12/1 13:45
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Person {
    private String name;
    private Integer age;
}
