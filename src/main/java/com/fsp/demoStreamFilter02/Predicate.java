package com.fsp.demoStreamFilter02;

/**
 * @author fushengping
 * @interface Predicate
 * @description 唯一函数接口
 * @date 2021/12/1 9:28
 */
@FunctionalInterface
public interface Predicate<T> {
    boolean test(T t);
}
