package com.fsp.demoStreamFilter02;

import java.util.ArrayList;
import java.util.List;

/**
 * @author fushengping
 * @className MyList
 * @description
 * @date 2021/12/1 10:27
 */
class MyList<T>{
    private List<T> list = new ArrayList<>();

    // 1.外部调用add()添加的元素都会被存在list
    public boolean add(T t) {
        return list.add(t);
    }

    /**
     * 过滤方法，接收过滤规则
     * filter(Predicate predicate)方法需要一个过滤规则，但这个规则不能写死，所以随便搞了一个接口占坑
     * @param predicate
     * @return
     */
    public List<T> filter(Predicate<T> predicate){
        List<T> filteredList = new ArrayList<>();

        for (T t : list) {
            // 2.把规则应用于list
            if (predicate.test(t)) {
                // 3.收集符合条件的元素
                filteredList.add(t);
            }
        }

        return filteredList;
    }
}
