package com.fsp.streamAPITest01;

import lombok.*;

/**
 * @author fushengping
 * @className Person
 * @description
 * @date 2021/12/3 16:54
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Person {
    private String name;
    private Integer age;
    private String address;
    private Double salary;

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", address='" + address + '\'' +
                ", salary=" + salary +
                '}';
    }
}
